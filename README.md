# Robust-ish error handling: non-happy flow as a first-class citizen

The presentation is hosted at http://tjalling.ran.gitlab.io/robust-ish-error-handling.

This is an HTTP URL, not an HTTPS URL, because `tjalling.ran.gitlab.com` is a sub-subdomain of `gitlab.io` and `gitlab.io`'s HTTPS certificate is not valid for sub-subdomains.
